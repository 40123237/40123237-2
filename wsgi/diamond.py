#@+leo-ver=5-thin
#@+node:ppython.20131221094631.1649: * @file diamond.py
#coding: utf-8
# c1w7_1.py, 這個程式示範如何將菱形列印程式由單機搬到 OpenShift


#@@language python
#@@tabwidth -4

#@+<<declarations>>
#@+node:ppython.20131221094631.1650: ** <<declarations>> (diamond)
import os
import cherrypy
#@-<<declarations>>
#@+others
#@+node:ppython.20131221094631.1651: ** 列印星號
# 這個自訂函式呼叫時以輸入變數控制後續的列印內容
def 列印星號(輸入變數):
    outstring = ""
    # 數列1 為[0, 1, ... 輸入變數-1]
    數列1 = list(range(輸入變數))
    # 數列2 為數列1 各元素加上輸入變數-1, 目的在移動位置
    數列2 = [x+輸入變數-1 for x in 數列1]
    # 反數列2 為數列1 元素反置, 也就是[輸入變數-1, ..., 0]
    反數列2 = reversed(數列1)
    # 利用 zip() 將 數列2 與反數列2 組成並列元組, 目的在同一行列印時能夠同時列出兩數列所宣告的位置
    集合 = zip(數列2, 反數列2)
    # 利用迴圈逐行列印, 並以元組索引的元素判斷列印位置
    for 索引 in 集合:
        for 數 in range(輸入變數*2-1):
            if 數 == 索引[0] or 數 == 索引[1]:
                outstring += "@"
            else:
                outstring += "1"
        outstring += "<br />"
    return outstring
#@+node:ppython.20131221094631.1652: ** 列印星號2
def 列印星號2(輸入變數):
    outstring = ""
    數列1 = list(range(輸入變數))
    數列2 = [x+輸入變數 for x in 數列1]
    數列3 = [x+1 for x in 數列1]
    反數列2 = reversed(數列2)
    集合 = zip(數列3, 反數列2)
    for 索引 in 集合:
        for 數 in range(輸入變數*2+1):
            if 數 == 索引[0] or 數 == 索引[1]:
                outstring += "@"
            else:
                outstring += "1"
        outstring += "<br />"
    return outstring
#@+node:ppython.20131221094631.1653: ** class Diamond
class Diamond(object):
    #@+others
    #@+node:ppython.20131221094631.1654: *3* index
    @cherrypy.expose
    def index(self, num=5):
        outstring = ""
        outstring += 列印星號(int(num))
        outstring += 列印星號2(int(num)-1)
        return outstring+"<br /><br /><a href='/'>回首頁</a>"
    #@-others
#@-others
if __name__ == '__main__':
    # 假如在 os 環境變數中存在 'OPENSHIFT_REPO_DIR', 表示程式在 OpenShift 環境中執行
    if 'OPENSHIFT_REPO_DIR' in os.environ.keys():
        # 雲端執行啟動
        application = cherrypy.Application(Diamond())
    else:
        # 近端執行啟動
        '''
        cherrypy.server.socket_port = 8083
        cherrypy.server.socket_host = '127.0.0.1'
        '''
        cherrypy.quickstart(Diamond())
#@-leo
